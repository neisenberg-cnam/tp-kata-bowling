using System.Text.Json;
using Bowling;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Web;
using Microsoft.AspNetCore.Mvc.ViewFeatures;

namespace API.Controllers;

[ApiController]
[Route("[controller]")]
public class BowlingController : ControllerBase
{

    private readonly ILogger<BowlingController> _logger;
    private readonly IHttpContextAccessor _context;
    private Bowling.Bowling BowlingGame;

    private static string GameKey = "Game";

    public BowlingController(ILogger<BowlingController> logger, IHttpContextAccessor context)
    {
        _logger = logger;
        _context = context;
        var serializedObj = context.HttpContext.Session.GetString(GameKey);
        if (String.IsNullOrEmpty(serializedObj))
        {
            BowlingGame = new Bowling.Bowling();
        }
        else
        {
            var obj = JsonConvert.DeserializeObject<Bowling.Bowling>(serializedObj, new JsonSerializerSettings
            {
                ObjectCreationHandling = ObjectCreationHandling.Replace
            });
            if(obj != null)
                BowlingGame = obj;
            else
                BowlingGame = new Bowling.Bowling();
        }
    }

    [HttpPost(Name = "Throw")]
    public IActionResult ThrowBall(int nbPinsHit)
    {
        try
        {
            BowlingGame.PlayerThrow(nbPinsHit);
        }
        catch (NegativePinsException e)
        {
            return StatusCode(403, "Issue with pins number");
        }
        
        _context.HttpContext.Session.SetString(GameKey, JsonConvert.SerializeObject(BowlingGame));

        var dict = new Dictionary<string, object>()
        {
            {"TotalScore", BowlingGame.ScoreManager.GetTotalScore()},
            {"Pins", BowlingGame.CurrentFrame.pins.getPins()},
            {"ScoreBoard", BowlingGame.ScoreManager.GetScoreByRound()}
        };

        return Ok(dict);
    }

    [HttpGet(Name = "NewGame")]
    public IActionResult NewGame()
    {
        BowlingGame.NewGame();
        _context.HttpContext.Session.SetString(GameKey, JsonConvert.SerializeObject(BowlingGame));
        return Ok("Ok");
    }
}