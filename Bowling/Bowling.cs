﻿namespace Bowling;

public class Bowling
{
    public Frame CurrentFrame { get; set; }
    public int CurrentRoundNumber { get; set; }
    public int[] CurrentResults { get; set; }
    public bool IsSecondThrow { get; set; }
    public ScoreManager ScoreManager { get; set; }

    public Bowling()
    {
        NewGame();
    }

    public void NewGame()
    {
        CurrentFrame = new Frame();
        CurrentRoundNumber = 1;
        ScoreManager = new ScoreManager();
        CurrentResults = new int[2];
        IsSecondThrow = false;
    }

    public void PlayerThrow(int nbPinsHits)
    {
        CurrentFrame.Throw(nbPinsHits);
        if (!IsSecondThrow)
        {
            CurrentResults[0] = nbPinsHits;
            if (nbPinsHits == 10)
            {
                GoToNextRound();
            }
            else
            {
                IsSecondThrow = true;
            }
        }
        else
        {
            CurrentResults[1] = nbPinsHits;
            GoToNextRound();
        }
    }

    private void GoToNextRound()
    {
        CurrentFrame = new Frame();
        ScoreManager.ChangeNthRoundResult(CurrentRoundNumber, new FrameResults(CurrentResults[0], CurrentResults[1]));
        IsSecondThrow = false;
        CurrentResults[0] = 0;
        CurrentResults[1] = 0;
        CurrentRoundNumber++;
    }

    public Dictionary<string, int> ToDict()
    {
        return new Dictionary<string, int>()
        {
            {"totalScore", ScoreManager.GetTotalScore()},
            {"totalScore", ScoreManager.GetTotalScore()}
        };
    }
}