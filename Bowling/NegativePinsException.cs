using System.Runtime.Serialization;

namespace Bowling;

public class NegativePinsException : Exception
{
    public NegativePinsException() {}

    public NegativePinsException(string? message) : base(message) {}
        
    public NegativePinsException(string? message, Exception? innerException) : base(message, innerException) {}

    protected NegativePinsException(SerializationInfo info, StreamingContext context) : base(info, context) {}

}