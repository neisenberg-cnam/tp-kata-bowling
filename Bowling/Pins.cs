using Newtonsoft.Json;

namespace Bowling;

public class Pins
{
    [JsonProperty]
    private int pins { get; set; }

    public Pins()
    {
        pins = 10;
    }

    public int getPins()
    {
        return pins;
    }

    public void removePins(int n)
    {
        if (pins < n)
        {
            throw new NegativePinsException("Pins can't be at a negative value");
        }
        else
            pins -= n;
    }

    public void setPins(int n)
    {
        if (n < 0)
            throw new NegativePinsException("Pins can't be at negative value");
        if (n > 10)
            throw new OverTenPinsException("Pins can't be more than 10");
        
    }
}