using Newtonsoft.Json;

namespace Bowling;

public class ScoreManager
{
    [JsonProperty]
    public List<FrameResults> scores;

    public ScoreManager()
    {
        if (scores == null || scores.Count == 0)
        {
            scores = new List<FrameResults>();
            var nullFrame = new FrameResults(0, 0);
            for (int i = 0; i < 11; i++)
            {
                AddFrameResults(nullFrame);
            }
        }
    }

    public void AddFrameResults(FrameResults frame)
    {
        if(scores.Count < 11)
            scores.Add(frame);
    }

    public void ChangeNthRoundResult(int index, FrameResults frameResult)
    {
        scores[index - 1] = frameResult;
    }

    public Dictionary<int, int> GetScoreByRound()
    {
        Dictionary<int, int> dict = new Dictionary<int, int>();
        for (int i = 0; i < scores.Count - 1; i++)
        {
            dict[i+1] = GetScoreForARound(scores[i], scores[i + 1]);
        }

        return dict;
    }

    public int GetTotalScore()
    {
        int total = 0;
        for (int i = 0; i < scores.Count - 1; i++)
        {
            total += GetScoreForARound(scores[i], scores[i + 1]);
        }

        return total;
    }

    public int GetScoreForARound(FrameResults current, FrameResults next)
    {
        int total = current.GetTotal();
        if (current.IsASpareOrAbove())
            total += 2 * next.GetFirstThrow();
        if (current.IsAStrike())
            total += 2 * next.GetSecondThrow();
        return total;
    }
}