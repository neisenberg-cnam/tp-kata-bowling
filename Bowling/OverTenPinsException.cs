using System.Runtime.Serialization;

namespace Bowling;

public class OverTenPinsException : Exception
{
    public OverTenPinsException() {}

    public OverTenPinsException(string? message) : base(message) {}
        
    public OverTenPinsException(string? message, Exception? innerException) : base(message, innerException) {}

    protected OverTenPinsException(SerializationInfo info, StreamingContext context) : base(info, context) {}

}