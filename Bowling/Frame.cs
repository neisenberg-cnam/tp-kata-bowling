namespace Bowling;

public class Frame
{
    public Pins pins;

    public Frame()
    {
        pins = new Pins();
    }

    public int Throw(int nbPinsHit)
    {
        pins.removePins(nbPinsHit);
        return pins.getPins();
    }
}