using Newtonsoft.Json;

namespace Bowling;

public class FrameResults
{
    [JsonProperty]
    private Tuple<int, int> results;

    public FrameResults(int throw1, int throw2)
    {
        results = new Tuple<int, int>(throw1, throw2);
    }

    public int GetTotal()
    {
        return results.Item1 + results.Item2;
    }

    public int GetFirstThrow()
    {
        return results.Item1;
    }
    
    
    public int GetSecondThrow()
    {
        return results.Item2;
    }
    
    public bool IsASpareOrAbove()
    {
        return results.Item1 + results.Item2 >= 10;
    }

    public bool IsAStrike()
    {
        return results.Item1 >= 10;
    }
    
    public bool IsASpare()
    {
        return IsASpareOrAbove() && results.Item2 != 0;
    }
}