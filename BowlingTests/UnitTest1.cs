using System.Collections.Generic;
using Xunit;

namespace BowlingTests;

using Bowling;

public class UnitTest1
{
    [Fact]
    public void FrameInit()
    {
        var Frame = new Frame();
        Assert.Equal(10, Frame.pins.getPins());
    }

    [Fact]
    public void FrameThrow()
    {
        var Frame = new Frame();
        Frame.Throw(5);
        Assert.Equal(5, Frame.pins.getPins());
    }

    [Fact]
    public void FrameThrowError()
    {
        var Frame = new Frame();
        Assert.Throws<NegativePinsException>(() => Frame.Throw(11));
    }

    [Fact]
    public void FrameResultsNormal()
    {
        var Frame1Result = new FrameResults(5, 2);
       
        Assert.False(Frame1Result.IsASpare());
        Assert.False(Frame1Result.IsASpareOrAbove());
        Assert.False(Frame1Result.IsAStrike());
    }
    
    
    [Fact]
    public void FrameResultsSpare()
    {
        var FrameSpareResult = new FrameResults(5, 5);
       
        Assert.True(FrameSpareResult.IsASpare());
        Assert.True(FrameSpareResult.IsASpareOrAbove());
        Assert.False(FrameSpareResult.IsAStrike());
    }
    
    
    [Fact]
    public void FrameResultsStrike()
    {
        var FrameStrikeResult = new FrameResults(10, 0);
       
        Assert.False(FrameStrikeResult.IsASpare());
        Assert.True(FrameStrikeResult.IsASpareOrAbove());
        Assert.True(FrameStrikeResult.IsAStrike());
    }

    [Fact]
    public void ScoreForAFrame()
    {
        var Frame1Result = new FrameResults(5, 2);
        var FrameSpareResult = new FrameResults(5, 5);
        var FrameStrikeResult = new FrameResults(10, 0);

        var ScoreManager = new ScoreManager();
        
        Assert.Equal(7, ScoreManager.GetScoreForARound(Frame1Result, Frame1Result));
        Assert.Equal(20, ScoreManager.GetScoreForARound(FrameSpareResult, Frame1Result));
        Assert.Equal(24, ScoreManager.GetScoreForARound(FrameStrikeResult, Frame1Result));
    }
    
    [Fact]
    public void ScoreStrikeBackToBack()
    {
        var FrameStrikeResult = new FrameResults(10, 0);

        var ScoreManager = new ScoreManager();
        
        Assert.Equal(30, ScoreManager.GetScoreForARound(FrameStrikeResult, FrameStrikeResult));
    }

    [Fact]
    public void ScoreTotalNormal()
    {
        var ScoreManager = new ScoreManager();
        var Frame1Result = new FrameResults(5, 2);
        
        for (int i = 1; i < 12; i++)
        {
            ScoreManager.ChangeNthRoundResult(i, Frame1Result);
        }

        var dict = new Dictionary<int, int>()
        {
            {1, 7},
            {2, 7},
            {3, 7},
            {4, 7},
            {5, 7},
            {6, 7},
            {7, 7},
            {8, 7},
            {9, 7},
            {10, 7},
        };
        
        Assert.Equal(70, ScoreManager.GetTotalScore());
        Assert.Equal(dict, ScoreManager.GetScoreByRound());
    }
    
    [Fact]
    public void BowlingTestAllStrikes()
    {
        var Bowling = new Bowling();
        
        for (int i = 1; i < 12; i++)
        {
            Bowling.PlayerThrow(10);
        }
        
        Assert.Equal(300, Bowling.ScoreManager.GetTotalScore());
    }
    
    
    [Fact]
    public void BowlingTestAllSpares()
    {
        var Bowling = new Bowling();
        
        for (int i = 1; i < 22; i++)
        {
            Bowling.PlayerThrow(5);
        }
        Bowling.PlayerThrow(0);
        
        Assert.Equal(200, Bowling.ScoreManager.GetTotalScore());
    }
    
    [Fact]
    public void BowlingTestAllNormal()
    {
        var Bowling = new Bowling();
        
        for (int i = 1; i < 21; i++)
        {
            Bowling.PlayerThrow(2);
        }
        
        Assert.Equal(40, Bowling.ScoreManager.GetTotalScore());
    }
}